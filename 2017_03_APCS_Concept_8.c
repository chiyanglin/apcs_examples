/*

8. 給定右側程式，其中 s 有被宣告為全域變數，請問程式
執行後輸出為何？
(A) 1,6,7,7,8,8,9
(B) 1,6,7,7,8,1,9
(C) 1,6,7,8,9,9,9
(D) 1,6,7,7,8,9,9

*/

int s = 1; // 全域變數
void add (int a) {
 int s = 6;
 for( ; a>=0; a=a-1) {
   printf("%d,", s);
   s++;
   printf("%d,", s);
 }
}

int main () {
 printf("%d,", s);
 add(s);
 printf("%d,", s);
 s = 9;
 printf("%d", s);
 return 0;
}
