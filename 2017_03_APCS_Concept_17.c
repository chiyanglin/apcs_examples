/*
17. 給定右側函式 F()，F()執行完所回傳的 x 值為
何？
(A) n(n+1)√⌊log2 𝑛⌋
(B) n2(n+1)/2
(C) n(n+1)⌊log2n + 1⌋/2
(D) n(n+1)/2

*/

int F (int n) {
 int x = 0;
 for (int i=1; i<=n; i=i+1)
    for (int j=i; j<=n; j=j+1)
        for (int k=1; k<=n; k=k*2)
            x = x + 1;
 return x;
}
