/*

7. 若以 B(5,2)呼叫右側 B()函式，總共
會印出幾次 “base case”？
(A) 1
(B) 5
(C) 10
(D) 19

*/

int B (int n, int k) {
 if (k == 0 || k == n){
    printf ("base case\n");
    return 1;
 }
 return B(n-1,k-1) + B(n-1,k);
}
