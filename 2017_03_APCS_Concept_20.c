/*

20. 小藍寫了一段複雜的程式碼想考考你
是否了解函式的執行流程。請回答程式
最後輸出的數值為何？
(A) 70
(B) 80
(C) 100
(D) 190

*/

int g1 = 30, g2 = 20;
int f1(int v) {
 int g1 = 10;
 return g1+v;
}

int f2(int v) {
 int c = g2;
 v = v+c+g1;
 g1 = 10;
 c = 40;
 return v;
}

int main() {
 g2 = 0;
 g2 = f1(g2);
 printf("%d", f2(f2(g2)));
 return 0;
}
