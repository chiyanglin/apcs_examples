/*

14. 若以 F(15)呼叫右側 F()函式，總共會印出幾行
數字？
(A) 16 行
(B) 22 行
(C) 11 行
(D) 15 行

*/

void F (int n) {
   printf ("%d\n" , n);
   if ((n%2 == 1) && (n > 1)){
      return F(5*n+1);
   } else {
      if (n%2 == 0)
        return F(n/2);
   }
}
