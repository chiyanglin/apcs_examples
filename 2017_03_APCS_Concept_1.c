/*

1.

給定一個 1x8 的陣列 A， A = {0, 2, 4,
6, 8, 10, 12, 14}。右側函式
Search(x) 真正目的是找到 A 之中大於 x
的最小值。然而，這個函式有誤。請問下列哪
個函式呼叫可測出函式有誤？
(A) Search(-1)
(B) Search(0)
(C) Search(10)
(D) Search(16)

*/

int A[8]={0, 2, 4, 6, 8, 10, 12, 14};
int Search (int x) {
 int high = 7;
 int low = 0;
 while (high > low) {
   int mid = (high + low)/2;
   if (A[mid] <= x) {
      low = mid + 1;
    } else {
      high = mid;
    }
 }
 return A[high];
}
