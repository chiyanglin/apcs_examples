/*

6. 若 A[][] 是一個 MxN 的整
數陣列，右側程式片段用以
計算 A 陣列每一列的總和，
以下敘述何者正確？
(A) 第一列總和是正確，但其他列總和不一定正確
(B) 程式片段在執行時會產生錯誤 (run-time error)
(C) 程式片段中有語法上的錯誤
(D) 程式片段會完成執行並正確印出每一列的總和

*/

void main () {
 int rowsum = 0;
 for (int i=0; i<M; i=i+1) {
   for (int j=0; j<N; j=j+1) {
      rowsum = rowsum + A[i][j];
    }
    printf("The sum of row %d is %d.\n", i, rowsum);
 }
}
