/*

22. 若要邏輯判斷式 !(X1 || X2)計算結果為真(True)，則 X1 與 X2 的值分別應為何？

(A) X1 為 False，X2 為 False
(B) X1 為 True，X2 為 True
(C) X1 為 True，X2 為 False
(D) X1 為 False，X2 為 True


*/
